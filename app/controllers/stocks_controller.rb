class StocksController < ApplicationController

  def index
    @stocks = StockForm.new(params).result.page(params[:page]) if params[:matrix].blank?
    @rows = Kaminari.paginate_array(StockForm.new(params).matrix_result).page(params[:page]) if params[:matrix].present?
  end

  def new
  end

  def create
    if stocks_params[:codes].blank?
      render :new
    else
      stocks_params[:codes].lines.each do |code|
        params = { start_at: stocks_params[:start_at], end_at: stocks_params[:end_at], code: code.chomp }
        YahooCrawler.perform_now(params)
      end
      redirect_to stocks_path, notice: "株価を新規取得しました。"
    end
  end

  def csv
    if params[:matrix].blank?
      @stocks = StockForm.new(params).result
      generated_csv = CSV.generate do |csv|
        csv << %w(証券コード 日付 調整後終値)
        @stocks.find_each do |stock|
          stock_price = if params[:empty_correction]
            stock.update(empty_corrected_price: stock.prev_price) if stock.adjusted_closing_price.blank? && stock.empty_corrected_price.blank?
            stock.adjusted_closing_price || stock.empty_corrected_price
          else
            stock.adjusted_closing_price
          end
          csv << [
            stock.code,
            stock.date,
            stock_price
          ]
        end
      end
      send_data(generated_csv.encode(Encoding::CP932, invalid: :replace, undef: :replace), filename: "stocks.csv", type: "text/csv; charset=shift_jis")
    else
      @rows = StockForm.new(params).matrix_result
      generated_csv = CSV.generate do |csv|
        codes = @rows.first.map{ |content| content[:code].to_s }
        codes = codes.unshift("")
        csv << codes
        @rows.each do |row|
          array = [row.first[:date]]
          row.each do |content|
            stock = Stock.find_by(code: content[:code], date: content[:date])
            if stock.blank?
              array << [""]
            else
              array << if params[:empty_correction]
                stock.adjusted_closing_price || stock.empty_corrected_price
              else
                stock.adjusted_closing_price
              end
            end
          end
          csv << array
        end
      end
      send_data(generated_csv.encode(Encoding::CP932, invalid: :replace, undef: :replace), filename: "stocks.csv", type: "text/csv; charset=shift_jis", disposition: "attachment")
    end
  end

  private

  def stocks_params
    params.permit(:start_at, :end_at, :codes)
  end
end

class ErrorLogsController < ApplicationController

  def index
    @error_logs = ErrorLog.order(id: :desc).page(params[:page])
  end
end

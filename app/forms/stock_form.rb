class StockForm

  def initialize(params)
    @params = params
  end

  def result
    stocks = Stock.all
    stocks.where!(code: @params[:codes].lines.map{ |line| line.chomp.to_i }) if @params[:codes].present?
    if @params[:start_at].present?
      stocks.where!(":start_date <= date", start_date: @params[:start_at].to_date)
    end
    if @params[:end_at].present?
      stocks.where!("date <= :end_date", end_date: @params[:end_at].to_date)
    end

    case @params[:sort]
    when "2"
      stocks.order!(code: :asc).order!(date: :desc)
    when "3"
      stocks.order!(code: :desc).order!(date: :asc)
    when "4"
      stocks.order!(code: :desc).order!(date: :desc)
    else
      stocks.order!(code: :asc).order!(date: :asc)
    end

    stocks
  end


  def matrix_result
    stocks = Stock.all
    codes = @params[:codes].lines.map{ |line| line.chomp.to_i }.presence || Stock.pluck(:code).uniq
    stocks.where!(code: codes) if @params[:codes].present?
    if @params[:start_at].present?
      stocks.where!(":start_date <= date", start_date: @params[:start_at].to_date)
    end
    if @params[:end_at].present?
      stocks.where!("date <= :end_date", end_date: @params[:end_at].to_date)
    end

    start_date = @params[:start_at].to_date || stocks.order(date: :asc).first.try!(:date)
    end_date = @params[:end_at].to_date || stocks.order(date: :asc).last.try!(:date)
    dates = TradingDayJp.between(start_date, end_date) if start_date.present? && end_date.present?

    case @params[:sort]
    when "1"
      codes.sort!
    when "2"
      codes.sort!
      dates.reverse!
    when "3"
      codes.sort!.reverse!
    when "4"
      codes.sort!.reverse!
      dates.reverse!
    end

    return [] if dates.blank? || codes.blank?
    dates.map { |date| codes.map { |code| { date: date, code: code } } }
  end
end

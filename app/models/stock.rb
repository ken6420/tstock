class Stock < ActiveRecord::Base
  validates :code, presence: true, numericality: { only_integer: true }
  validates :date, presence: true
  validates :adjusted_closing_price, numericality: true, allow_blank: true
  validates :empty_corrected_price, numericality: true, allow_blank: true

  def prev_price
    prev_stock = Stock.find_by(code: code, date: TradingDayJp.prev(date))
    prev_stock.adjusted_closing_price || prev_stock.empty_corrected_price if prev_stock.present?
  end

  def self.find_or_create(attrs)
    find_by(code: attrs[:code], date: attrs[:date]).presence || create(attrs)
  end

  def self.update_or_create(attrs)
    if stock = find_by(code: attrs[:code], date: attrs[:date])
      stock.update(attrs)
    else
      create(attrs)
    end
  end
end

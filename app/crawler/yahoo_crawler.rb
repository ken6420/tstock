require 'open-uri'
require 'nokogiri'

class YahooCrawler < ActiveJob::Base
  queue_as :default

  attr_accessor :url, :doc, :params

  YAHOO_ROOT_PATH = "https://info.finance.yahoo.co.jp/history/"

  def perform(params)
    @params = params
    @code = @params[:code].to_i
    return if @code.zero?
    import_all!
    create_blank_stock
  end

  private

  def import_all!
    #start
    create_urls.each do |url|
      set_doc!(url)
      next if @doc.blank?
      import!
      sleep 1
    end
    #finish
  end

  def import!
    rows = @doc.search("table.boardFin.yjSt.marB6 > tr")
    import_rows(rows)
  end

  def import_rows(rows)
    handle_import(rows) do |row|
      attrs = fetch_attrs(row)
      next if attrs.blank?
      Stock.update_or_create(attrs)
    end
  end

  def set_doc!(url)
    @url = url
    begin
      @doc = Nokogiri::HTML(open(url).read)
    rescue Encoding::UndefinedConversionError
      @doc = Nokogiri::HTML(open(url), nil, 'euc-jp')
    rescue => e
      params = self.params.merge({ type: "url_open_exception" })
      ErrorLog.create(params: params, error: e)
    end
  end

  def create_urls
    end_at = end_time
    start_at = start_time

    sy = start_at.year
    sm = start_at.month
    sd = start_at.day
    ey = end_at.year
    em = end_at.month
    ed = end_at.day

    #ページ数を計算
    first_url = "#{YAHOO_ROOT_PATH}?code=#{@code}&sy=#{sy}&sm=#{sm}&sd=#{sd}&ey=#{ey}&em=#{em}&ed=#{ed}&tm=d&p=1"
    set_doc!(first_url)
    total_page = get_total_page if @doc.present?

    return [] if total_page.blank?

    1.upto(total_page).map do |page|
      "#{YAHOO_ROOT_PATH}?code=#{@code}&sy=#{sy}&sm=#{sm}&sd=#{sd}&ey=#{ey}&em=#{em}&ed=#{ed}&tm=d&p=#{page}"
    end
  end

  def end_time
    @params[:end_at].present? ? Time.zone.parse(@params[:end_at]) : Time.now
  end

  def start_time
    @params[:start_at].present? ? Time.zone.parse(@params[:start_at]) : end_time - 1.month
  end

  def get_total_page
    str = @doc.at("span.stocksHistoryPageing.yjS").try!(:text)
    (str.match(%r{件/(.+?)件中})[1].to_i / 20.0).ceil if str.present?
  end

  def fetch_attrs(row)
    tds = row.search("td")
    attrs = { code: @code }
    date = tds[0].try!(:text)
    attrs[:date] = date.strip.gsub(/年/, "-").gsub(/月/, "-").gsub(/日/, "") if date.present?
    price = tds[6].try!(:text)
    attrs[:adjusted_closing_price] = price.gsub(/,/, "").to_f if price.present?
    return if attrs[:date].blank? || attrs[:adjusted_closing_price].blank? || attrs[:adjusted_closing_price].zero?
    attrs
  end

  def handle_import(rows)
    rows.each do |row|
      begin
        ActiveRecord::Base.transaction do
          yield(row)
        end
      rescue => e
        next if e.message.match(/^400 Bad Request$/)
        next if e.message.match(/^404 Not Found$/)
        params = self.params.merge({ type: "import_exception" })
        ErrorLog.create(params: params, error: e)
      end
    end
  end

  def create_blank_stock
    end_date = end_time.to_date
    start_date = start_time.to_date
    TradingDayJp.between(start_date, end_date).each do |date|
      attrs = { code: @code, date: date }
      stock = Stock.find_or_create(attrs)
      stock.update(empty_corrected_price: stock.prev_price) if stock.adjusted_closing_price.blank?
    end
  end
end

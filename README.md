## TSTOCK
株価を取得できます。
last update: 2017/11/01

## 設定
### 環境構築
#### ミドルウェアのインストール・設定
OSはMac OS X Sierraを想定しています。既にインストールしているものは飛ばして大丈夫です。
ターミナルを起動して、以下を順次実行してください。

**git**
```shell
$ git config --global user.name "YOUR NAME"
$ git config --global user.email your_email@example.com
```

**homebrew**
```shell
$ sudo chown -R $(whoami):admin /usr/local
$ /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

**SSH鍵**
```shell
$ cd ~/
$ mkdir .ssh
$ cd ~/.ssh
$ ssh-keygen -t rsa -b 4096 -C "メールアドレス"
$ eval "$(ssh-agent -s)"
$ ssh-add ~/.ssh/id_rsa
```

**MySQL**
```shell
$ brew install mysql\@5.7
$ brew link mysql\@5.7 --force
$ brew services start mysql@5.7 # 自動起動設定
$ mysql_secure_installation
$ mysql -h 127.0.0.1 -u root -p # 起動確認（終了はexit）
```

**rbenv**
```shell
$ brew install rbenv

$ echo 'eval "$(rbenv init -)"' >> ~/.bash_profile
$ source ~/.bash_profile
$ rbenv install -l
$ rbenv install 2.3.1
$ rbenv global 2.3.1
$ rbenv versions
```

**リポジトリのクローン**
```shell
$ cd [リポジトリを作成したい上位ディレクトリ]
$ git clone git@bitbucket.org:ken6420/tstock.git
$ git checkout master
```
git cloneをしたディレクトリの直下に/tstockが作成されます。

**Ruby**
```shell
$ cd tstock
$ ruby -v # 2.3.1

# 2.3.1ではない場合
$ rbenv local 2.3.1
```

**アプリケーションの設定**
```shell
# ライブラリのインストール
$ gem install bundler
$ bundle install --path=vendor/bundle

# ファイル設定
$ vim config/database.yml
# (適当なエディタでconfig/database.ymlを開きます。各自のMySQL環境に合わせて、developmentのusernameとpasswardを設定してください。)

# データベース作成 & マイグレーション
$ bundle exec rake db:create
$ bundle exec rake db:migrate

# ローカルサーバー起動
$ bundle exec rails s

# ローカルサーバーの終了は control+c
```
これで適当なブラウザを開き http://localhost:3000/ にアクセスすると使用することができます。

require "rails_helper"
require "webmock/rspec"

EXAMPLE_PARAMS = { code: 4649, start_at: "2016-10-01", end_at: "2016-11-07" }
EXAMPLE_URL = "http://info.finance.yahoo.co.jp/history/?code=4649&ed=7&em=11&ey=2016&p=1&sd=1&sm=10&sy=2016&tm=d"

describe YahooCrawler do
  describe "#perform_now" do

    let(:crawl) { YahooCrawler.perform_now(EXAMPLE_PARAMS) }

    before do
      # テストデータ(2016/11/07の実データ)をスタブで取り込み
      File.open(File.join(Rails.root, "/spec/fixtures/html/yahoo.html")) do |f|
        stub_request(:get, EXAMPLE_URL).to_return({body: f, status: 200})
      end
    end

    context "例外が起こらなかった場合" do
      it "24件取り込みできる" do
        crawl
        expect(Stock.count).to eq 24
        expect(Stock.first.code).to eq 4649.0
        expect(Stock.first.date.to_s).to eq "2016-11-04"
        expect(Stock.first.adjusted_closing_price).to eq 744
        expect(Stock.last.code).to eq 4649.0
        expect(Stock.last.date.to_s).to eq "2016-11-07"
      end

      it "4件は調整後終値が得られない" do
        crawl
        expect(Stock.where(adjusted_closing_price: nil).count).to eq 4
      end

      it "調整後終値が得られなかった場合、補正価格が得られる" do
        crawl
        expect(Stock.where.not(empty_corrected_price: nil).count).to eq 4
        expect(Stock.last.empty_corrected_price).to eq 744.0
      end
    end
  end
end

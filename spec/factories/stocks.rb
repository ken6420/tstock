FactoryGirl.define do
  factory :stock do
    code 1234
    date "2000-01-01"
    adjusted_closing_price 10000
    empty_corrected_price nil
  end
end

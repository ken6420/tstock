class CreateErrorLogs < ActiveRecord::Migration
  def change
    create_table :error_logs do |t|
      t.string :params
      t.string :error

      t.timestamps null: false
    end
  end
end

class CreateStocks < ActiveRecord::Migration
  def change
    create_table :stocks do |t|
      t.integer :code, null: false
      t.date :date, null: false
      t.float :adjusted_closing_price
      t.float :empty_corrected_price

      t.timestamps null: false
    end
    add_index :stocks, [:code, :date]
  end
end

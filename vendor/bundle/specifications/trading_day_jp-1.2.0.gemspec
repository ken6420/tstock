# -*- encoding: utf-8 -*-
# stub: trading_day_jp 1.2.0 ruby lib

Gem::Specification.new do |s|
  s.name = "trading_day_jp"
  s.version = "1.2.0"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib"]
  s.authors = ["Takehiko Shinkura"]
  s.date = "2015-04-11"
  s.email = ["tynmarket@gmail.com"]
  s.homepage = "https://github.com/tyn-iMarket/trading_day_jp.git"
  s.licenses = ["MIT"]
  s.rubygems_version = "2.5.1"
  s.summary = "Trading days for Japanese stock market"

  s.installed_by_version = "2.5.1" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<holiday_jp>, [">= 0.4"])
      s.add_development_dependency(%q<bundler>, ["~> 1.6"])
      s.add_development_dependency(%q<rake>, [">= 0"])
      s.add_development_dependency(%q<rspec>, ["~> 3.0.0"])
    else
      s.add_dependency(%q<holiday_jp>, [">= 0.4"])
      s.add_dependency(%q<bundler>, ["~> 1.6"])
      s.add_dependency(%q<rake>, [">= 0"])
      s.add_dependency(%q<rspec>, ["~> 3.0.0"])
    end
  else
    s.add_dependency(%q<holiday_jp>, [">= 0.4"])
    s.add_dependency(%q<bundler>, ["~> 1.6"])
    s.add_dependency(%q<rake>, [">= 0"])
    s.add_dependency(%q<rspec>, ["~> 3.0.0"])
  end
end

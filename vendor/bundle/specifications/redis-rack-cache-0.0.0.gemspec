# -*- encoding: utf-8 -*-
# stub: redis-rack-cache 0.0.0 ruby lib

Gem::Specification.new do |s|
  s.name = "redis-rack-cache"
  s.version = "0.0.0"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib"]
  s.authors = ["Luca Guidi"]
  s.date = "2011-09-08"
  s.description = "Redis for Rack::Cache"
  s.email = ["guidi.luca@gmail.com"]
  s.homepage = "http://jodosha.github.com/redis-store"
  s.rubyforge_project = "redis-rack-cache"
  s.rubygems_version = "2.5.1"
  s.summary = "Redis for Rack::Cache"

  s.installed_by_version = "2.5.1" if s.respond_to? :installed_by_version
end

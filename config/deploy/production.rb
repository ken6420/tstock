server "54.250.113.195", user: "deploy", roles: %w{app db web}, primary: true
set :branch,    "master"
set :stage,     :production
set :rails_env, :production
